module.exports = {
  apimsgconfig: {
    en: {
      200: 'Record(s) Found',
      201: 'No record(s) found',
      202: 'Inserted successfully',
      203: 'Updated successfully',
      204: 'Deleted successfully',
      205: 'Feedback sent successfully',
      206: 'Your cart is empty',

      /* user codes */
      213: 'Login successfully',

      /* error codes */
      400: 'Invalid request',
      401: 'Invalid auth token',
      402: 'Record not found',
      403: 'Unable to process now, Please try again later',
      404: 'Invalid MIME Type',
      405: 'Token expired/invalid',

      /* user codes */
      411: 'Mobile number is required',
      412: 'Invalid mobile number',
      413: 'Mobile number already exist',
      414: 'Mobile number not registered',
      599: 'Internal server error'

      // 202: 'Required username'
    }
  }

}
