const response = require('./response')
const success = require('./success')
const auth = require('./auth')
const errors = require('./errors')
const headers = require('./header')
const user = require('./user')

module.exports = {
  Success: success,
  Errors: errors,
  Request: response,
  Response: response,
  Auth: auth,
  Headers: headers,
  User: user
}
