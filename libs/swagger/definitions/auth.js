module.exports = {
  authenticate: {
    required: [
      'email',
      'password'
    ],
    properties: {
      email: {
        type: 'string',
        example: 'manager@shenll.com'
      },
      password: {
        type: 'string',
        example: 'your@password'
      }
    }
  }
}
