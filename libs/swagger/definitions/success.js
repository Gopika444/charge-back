const appConst = require('../../constants')

module.exports = {
  createUser: {
    description: appConst.MSG.CREATE,
    schema: {
      type: 'object',
      properties: {
        status: {
          type: 'string',
          example: 'Ok'
        },
        message: {
          type: 'string',
          example: 'Inserted sucessfully'
        },
        code: {
          type: 'number',
          example: '202'
        },
        data: {
          type: 'array',
          item: 'string',
          example: [{
            _id: '615c293fea7cac46c414b111',
            companyId: [],
            userName: 'shenll',
            email: 'shenll@gmail.com',
            panCardNumber: 'ALWPG5809L',
            aadharNumber: '1234 5678 9123',
            mobileNumber: '7891841170',
            gpayNumber: '9856085690',
            role: 'FRANCHISE',
            status: 'ACTIVE',
            image: '/path/fieldName/image.png',
            isSubscriber: 'YES',
            country: 'India',
            state: 'hdghf',
            city: 'sdmsnd',
            password: '$2b$08$l788Qf5FXwZ02ojx2DLzsuEHQNatEjVVXxfNelBeX9vgVDjMXumNG',
            area: 'skjHAS',
            isVerified: '0',
            createdAt: '2021-10-05T10:30:23.193Z',
            updatedAt: '2021-10-05T10:30:23.193Z',
            referralCode: []
          }
          ]
        }
      }
    }
  },
  getUser: {
    description: appConst.MSG.HAS_RECORD,
    schema: {
      type: 'object',
      properties: {
        status: {
          type: 'string',
          example: 'Ok'
        },
        message: {
          type: 'string',
          example: 'Records found'
        },
        code: {
          type: 'number',
          example: '200'
        },
        data: {
          type: 'array',
          item: 'string',
          example: [{
            status: 'ok',
            message: 'Records found',
            data: [
              {
                _id: '615c293fea7cac46c414b111',
                companyId: [],
                userName: 'shenll',
                email: 'shenll@gmail.com',
                panCardNumber: 'ALWPG5809L',
                mobileNumber: '7891841170',
                aadharNumber: '1234 5678 9123',
                gpayNumber: '9856085690',
                role: 'FRANCHISE',
                status: 'ACTIVE',
                image: '/path/fieldName/image.png',
                isSubscriber: 'YES',
                country: 'India',
                state: 'hdghf',
                city: 'sdmsnd',
                password: '$2b$08$l788Qf5FXwZ02ojx2DLzsuEHQNatEjVVXxfNelBeX9vgVDjMXumNG',
                area: 'skjHAS',
                isVerified: '0',
                createdAt: '2021-10-05T10:30:23.193Z',
                updatedAt: '2021-10-05T10:30:23.193Z',
                referralCode: []
              }
            ]
          }
          ]
        }
      }
    }
  },
  updateUser: {
    description: appConst.MSG.UPDATE,
    schema: {
      type: 'object',
      properties: {
        status: {
          type: 'string',
          example: 'Ok'
        },
        message: {
          type: 'string',
          example: 'Records found'
        },
        code: {
          type: 'number',
          example: '203'
        },
        data: {
          type: 'array',
          item: 'string',
          example: [{
            status: 'ok',
            message: 'updated sucessfully',
            data: [
              {
                _id: '615c293fea7cac46c414b111',
                companyId: [],
                userName: 'shenll',
                aadharNumber: '1234 5678 9123',
                email: 'shenll@gmail.com',
                panCardNumber: 'ALWPG5809L',
                mobileNumber: '7891841170',
                gpayNumber: '9856085690',
                role: 'FRANCHISE',
                status: 'ACTIVE',
                image: '/path/fieldName/image.png',
                isSubscriber: 'YES',
                country: 'India',
                state: 'hdghf',
                city: 'sdmsnd',
                password: '$2b$08$l788Qf5FXwZ02ojx2DLzsuEHQNatEjVVXxfNelBeX9vgVDjMXumNG',
                area: 'skjHAS',
                isVerified: '0',
                createdAt: '2021-10-05T10:30:23.193Z',
                updatedAt: '2021-10-05T10:30:23.193Z',
                referralCode: []
              }
            ]
          }
          ]
        }
      }
    }
  },
  deleteUser: {
    description: appConst.MSG.REMOVE,
    schema: {
      type: 'object',
      properties: {
        status: {
          type: 'string',
          example: 'Ok'
        },
        message: {
          type: 'string',
          example: 'Records found'
        },
        code: {
          type: 'number',
          example: '204'
        },
        data: {
          type: 'array',
          item: 'string',
          example: [{
            status: 'ok',
            message: 'Deleted sucessfully',
            data: [
              {
                _id: '615c293fea7cac46c414b111',
                companyId: [],
                userName: 'shenll',
                email: 'shenll@gmail.com',
                panCardNumber: 'ALWPG5809L',
                mobileNumber: '7891841170',
                aadharNumber: '1234 5678 9123',
                gpayNumber: '9856085690',
                role: 'FRANCHISE',
                status: 'ACTIVE',
                image: '/path/fieldName/image.png',
                isSubscriber: 'YES',
                country: 'India',
                state: 'hdghf',
                city: 'sdmsnd',
                password: '$2b$08$l788Qf5FXwZ02ojx2DLzsuEHQNatEjVVXxfNelBeX9vgVDjMXumNG',
                area: 'skjHAS',
                isVerified: '0',
                createdAt: '2021-10-05T10:30:23.193Z',
                updatedAt: '2021-10-05T10:30:23.193Z',
                referralCode: []
              }
            ]
          }
          ]
        }
      }
    }
  },
  login: {
    description: appConst.MSG.LOGIN,
    schema: {
      type: 'object',
      properties: {
        status: {
          type: 'string',
          example: 'Ok'
        },
        message: {
          type: 'string',
          example: 'Logged in sucessfully'
        },
        code: {
          type: 'number',
          example: '412'
        },
        token: {
          type: 'string',
          example: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYx'
        }
      }
    }
  },
  noRecords: {
    description: appConst.MSG.NO_RECORD,
    schema: {
      type: 'object',
      properties: {
        status: {
          type: 'string',
          example: 'Ok'
        },
        code: {
          type: 'number',
          example: '201'
        },
        message: {
          type: 'string',
          example: 'No records found'
        }
      }
    }
  },
  updateArea: {
    description: appConst.MSG.UPDATE,
    schema: {
      type: 'object',
      properties: {
        status: {
          type: 'string',
          example: 'Ok'
        },
        message: {
          type: 'string',
          example: 'Records found'
        },
        code: {
          type: 'number',
          example: '203'
        },
        data: {
          type: 'array',
          item: 'string',
          example: [{
            status: 'ok',
            message: 'updated sucessfully',
            data: [{
              _id: '614f3a6b493cfb6dd7230092',
              name: 'A.Vellalapatti',
              cityId: {
                _id: '614f3526493cfb6dd722fe4d',
                name: 'Madurai',
                status: 'ACTIVE',
                stateId: {
                  _id: '614f2fa3493cfb6dd722fe48',
                  name: 'Tamil Nadu',
                  status: 'ACTIVE'
                }
              },
              stateId: {
                _id: '614f2fa3493cfb6dd722fe48',
                name: 'Tamil Nadu',
                status: 'ACTIVE'
              },
              status: 'ACTIVE'
            }
            ]
          }
          ]
        }
      }
    }
  },
  properties: {
    status: {
      type: 'string'
    },
    message: {
      type: 'string'
    },
    data: {
      type: 'array'
    },
    code: {
      type: 'number'
    }
  }
}
