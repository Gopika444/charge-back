module.exports = {
  create: {
    required: [
      'userName',
      'mobileNumber',
      'email',
      'password',
      'status',
      'role',
      'deviceToken',
      'deviceType'
    ],
    properties: {
      userName: {
        type: 'string',
        example: 'shenll'
      },
      email: {
        type: 'string',
        example: 'shenll@gmail.com'
      },
      password: {
        type: 'string',
        example: 'Shenll@2020'
      },
      mobileNumber: {
        type: 'string',
        example: '9874563210'
      },
      status: {
        type: 'string',
        default: 'ACTIVE',
        enum: ['ACTIVE', 'INACTIVE', 'DELETED']
      },
      deviceToken: {
        type: 'string',
        example: 'shenll142'
      },
      deviceType: {
        type: 'string',
        example: 'DISPa2'
      },
      role: {
        type: 'string',
        enum: ['USER', 'ADMIN', 'CLIENT'],
        example: 'ADMIN'
      }
    }
  },
  update: {
    properties: {
      userName: {
        type: 'string',
        example: 'shenll'
      },
      email: {
        type: 'string',
        example: 'shenll@gmail.com'
      },
      password: {
        type: 'string',
        example: 'Shenll@2020'
      },
      mobileNumber: {
        type: 'string',
        example: '9874563210'
      },
      status: {
        type: 'string',
        default: 'ACTIVE',
        enum: ['ACTIVE', 'INACTIVE', 'DELETED']
      },
      deviceToken: {
        type: 'string',
        example: 'shenll142'
      },
      deviceType: {
        type: 'string',
        example: 'DISPa2'
      },
      role: {
        type: 'string',
        enum: ['USER', 'ADMIN', 'CLIENT'],
        example: 'ADMIN'
      }
    }
  }
}
