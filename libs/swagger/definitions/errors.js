const appConst = require('../../constants')

module.exports = {
  badRequest: {
    description: appConst.MSG.BAD_REQUEST,
    schema: {
      type: 'object',
      properties: {
        status: {
          type: 'string',
          example: 'error'
        },
        code: {
          type: 'number',
          example: '400'
        },
        message: {
          type: 'string',
          example: 'Bad request'
        }
      }
    }
  },
  unauthorized: {
    description: appConst.MSG.UNAUTHORIZED,
    schema: {
      type: 'object',
      properties: {
        status: {
          type: 'number',
          example: 'error'
        },
        code: {
          type: 'string',
          example: '401'
        },
        message: {
          type: 'string',
          example: 'Unauthorized'
        }
      }
    }
  },
  internalServerError: {
    description: appConst.MSG.TRY_AGAIN,
    schema: {
      type: 'object',
      properties: {
        status: {
          type: 'string',
          example: 'error'
        },
        code: {
          type: 'number',
          example: '599'
        },
        message: {
          type: 'string',
          example: 'Internal server error'
        }
      }
    }
  }
}
