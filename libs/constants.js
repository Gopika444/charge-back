module.exports = {
  HEADER: {
    CONTENT_TYPE: 'application/json'
  },
  ERROR: {
    CODE: 500,
    MSG: 'error'
  },
  SUCCESS: {
    CODE: 200,
    MSG: 'ok'
  },
  MSG: {
    HAS_RECORD: 'record(s) found',
    CREATE: 'Inserted sucessfully',
    REMOVE: 'Deleted sucessfully',
    UPDATE: 'Updated sucessfully',
    BAD_REQUEST: 'Bad request',
    UNAUTHORIZED: 'Unauthorized',
    NO_RECORD: 'No records found',
    TRY_AGAIN: 'Try again',
    LOGIN: 'logged in sucessfully'
  },
  STATUS: {
    ALL: 'ALL',
    ACTIVE: 'ACTIVE',
    INACTIVE: 'INACTIVE',
    DELETE: 'DELETED',
    VERIFIED: 'VERIFIED',
    UN_VERIFIED: 'UN_VERIFIED',
    EXPIRED: 'EXPIRED',
    SUSPENDED: 'SUSPENDED'
  },
  ROLES: {
    ADMIN: 'ADMIN',
    CLIENT: 'CLIENT',
    USER: 'USER'
  }
}
