const joi = require('@hapi/joi')

const read = joi.object().keys()

const list = joi.object().keys()

const create = joi.object().keys({
  userName: joi.string().regex(/^[a-z\s]+$/i).min(3).max(30).required(),
  email: joi.string().regex(/^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$/).required(),
  role: joi.string().valid('USER', 'ADMIN', 'CLIENT').required(),
  password: joi.string(),
  mobileNumber: joi.string().regex(/^[1-9]{1}[0-9]{9}$/).required(),
  status: joi.string().valid('ACTIVE', 'INACTIVE', 'DELETED').required(),
  deviceType: joi.string(),
  deviceToken: joi.string()
})

const update = joi.object().keys({
  userName: joi.string().regex(/^[a-z\s]+$/i).min(3).max(30),
  email: joi.string().regex(/^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$/),
  role: joi.string().valid('USER', 'ADMIN', 'CLIENT'),
  password: joi.string(),
  mobileNumber: joi.string().regex(/^[1-9]{1}[0-9]{9}$/),
  status: joi.string().valid('ACTIVE', 'INACTIVE', 'DELETED'),
  deviceType: joi.string(),
  deviceToken: joi.string()
})

const remove = joi.object().keys()

module.exports = {
  list,
  read,
  create,
  update,
  remove
}
