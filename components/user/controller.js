const _ = require('lodash')
const BaseController = require('../base/controller')
const { userService: service } = require('./service')
const { SUCCESS, ERROR } = require('../../libs/constants')
const { apimsgconfig } = require('../../libs/api_message')

class Controller extends BaseController {
  async createUser (req, res, next) {
    try {
      const data = await service.createUser(req.body)
      if (!_.isEmpty(data) && data._id) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en[202], code: '202', data: [data] })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en[400], code: '400' })
      }
    } catch (e) {
      next(e)
    }
  }

  async getUsers (req, res, next) {
    try {
      const result = await service.getAll(req)
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en[200], code: '200', data: result })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async getUserById (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.getById(id)
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en[200], code: '200', data: result })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en[201], code: '201' })
      }
    } catch (e) {
      next(e)
    }
  }

  async updateUser (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.updateUser({ _id: id }, req.body)
      if (!_.isEmpty(result) && result.nModified !== 0) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en[203], code: '203', data: result })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en[599], code: '599' })
      }
    } catch (e) {
      next(e)
    }
  }

  async deleteUser (req, res, next) {
    try {
      const { id } = req.params
      const result = await service.deleteUser({ _id: id }, { status: 'DELETED' })
      if (!_.isEmpty(result)) {
        this.sendResponse(req, res, SUCCESS.CODE, { message: apimsgconfig.en[204], code: '204', data: result })
      } else {
        this.sendResponse(req, res, ERROR.CODE, { message: apimsgconfig.en[599], code: '599' })
      }
    } catch (e) {
      next(e)
    }
  }
}
module.exports = Controller
