const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcrypt')

const schema = new Schema(
  {
    userName: {
      type: String,
      trim: true,
      required: true
    },
    email: {
      type: String,
      trim: true,
      required: true
    },
    password: {
      type: String,
      trim: true
    },
    mobileNumber: {
      type: String,
      trim: true,
      required: true
    },
    status: {
      type: String,
      enum: ['ACTIVE', 'INACTIVE', 'DELETED'],
      required: true
    },
    deviceToken: {
      type: String,
      trim: true
    },
    deviceType: {
      type: String,
      trim: true
    }
  },
  { timestamps: true, versionKey: false }
)

schema.pre('save', function hashPassword (next) {
  if (this.isModified('password')) {
    this.password = bcrypt.hashSync(this.password, bcrypt.genSaltSync(8))
  }
  next()
})

const User = mongoose.model('users', schema)
module.exports = User
