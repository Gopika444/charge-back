const bcrypt = require('bcrypt')
const User = require('./schema')
const { errLogger } = require('../../config/logger')
const ClientError = require('../../helpers/ClientError')

class Service {
  async createUser (req) {
    const user = await User.findOne({ mobileNumber: req.mobileNumber })
    if (!user) {
      const data = new User(req)
      return data.save().catch((e) => {
        errLogger.error({ method: 'user-save', message: e.message })
      })
    } else { throw new ClientError('Already signed up', '216') }
  }

  async loginAdminPortal (req) {
    const user = await User.findOne({ $or: [{ email: req.email }, { userName: req.email }] })
    if (user) {
      const validPassword = await bcrypt.compare(req.password, user.password).catch((e) => {
        errLogger.error({ method: 'admin-login', message: e.message })
      })
      if (validPassword === true) { return user } else { throw new ClientError('Username/Password invalid', '216') }
    } else { throw new ClientError('Username/Password invalid', '216') }
  }

  async getAll () {
    return User.find().sort({ createdAt: -1 }).lean().catch((e) => {
      errLogger.error({ method: 'user-getAll', message: e.message })
    })
  }

  async getById (id) {
    return User.findById(id).lean().catch((e) => {
      errLogger.error({ method: 'User-getById', message: e.message })
    })
  }

  async updateUser (id, updateParams) {
    return User.findOneAndUpdate(id, updateParams).catch((e) => {
      errLogger.error({ method: 'User-update', message: e.message })
    })
  }

  async deleteUser (id, updateParams) {
    return User.findOneAndUpdate(id, updateParams).catch((e) => {
      errLogger.error({ method: 'User-getById', message: e.message })
    })
  }
}

const userService = new Service()

module.exports = { Service, userService }
