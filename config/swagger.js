const definitions = require('../libs/swagger/definitions/index')

module.exports = {
  swagger: '2.0',
  info: {
    version: '1.0.0',
    title: 'CBZero Project',
    description: ''
  },
  host: `${process.env.HOST}`,
  basePath: '/api/v1',
  tags: [
    {
      name: 'Authentication',
      description: 'Generate Auth Token to access resources'
    },
    {
      name: 'User',
      description: 'API for Users'
    }
  ],
  schemes: `${process.env.SCHEMES}`.split(','),
  consumes: ['application/json'],
  produces: ['application/json'],
  securityDefinitions: {
    auth: {
      type: 'apiKey',
      description: 'JWT authorization of an API',
      name: 'Authorization',
      in: 'header'
    }
  },
  security: [
    { auth: [] }
  ],
  paths: {
    '/auth/admin': {
      post: {
        tags: ['Authentication'],
        description: 'Create new Auth Token to access resources',
        parameters: [
          {
            name: 'user',
            in: 'body',
            schema: {
              $ref: '#/definitions/Auth/authenticate'
            }
          }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/login'
          },
          500: {
            $ref: '#/definitions/Response/internalServerError'
          }
        }
      }
    },
    '/user': {
      post: {
        tags: ['User'],
        summary: 'Add a new User',
        description: '',
        operationId: 'createUser',
        parameters: [
          {
            name: 'user',
            in: 'body',
            schema: {
              $ref: '#/definitions/User/create'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/createUser'
          },
          400: {
            $ref: '#/definitions/Response/badRequest'
          },
          401: {
            $ref: '#/definitions/Response/unauthorized'
          },
          500: {
            $ref: '#/definitions/Response/internalServerError'
          }
        }
      },
      get: {
        tags: ['User'],
        summary: 'Find User',
        description: '',
        operationId: 'getUser',
        responses: {
          200: {
            $ref: '#/definitions/Response/getUser'
          },
          400: {
            $ref: '#/definitions/Response/badRequest'
          },
          401: {
            $ref: '#/definitions/Response/unauthorized'
          },
          201: {
            $ref: '#/definitions/Response/noRecords'
          },
          500: {
            $ref: '#/definitions/Response/internalServerError'
          }
        }
      }
    },
    '/user/{id}': {
      get: {
        tags: ['User'],
        summary: 'Find User by ID',
        description: 'Returns a single User',
        operationId: 'getUserById',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'Return single User data',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/getUser'
          },
          400: {
            $ref: '#/definitions/Response/badRequest'
          },
          401: {
            $ref: '#/definitions/Response/unauthorized'
          },
          201: {
            $ref: '#/definitions/Response/noRecords'
          },
          500: {
            $ref: '#/definitions/Response/internalServerError'
          }
        }
      },
      put: {
        tags: ['User'],
        summary: 'Update an existing User',
        description: '',
        operationId: 'updateUser',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'ID of User that needs to be updated',
            required: true,
            type: 'string'
          },
          {
            in: 'body',
            name: 'user',
            description: 'Updated User object',
            required: true,
            schema: {
              $ref: '#/definitions/User/update'
            }
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/updateUser'
          },
          400: {
            $ref: '#/definitions/Response/badRequest'
          },
          401: {
            $ref: '#/definitions/Response/unauthorized'
          },
          201: {
            $ref: '#/definitions/Response/noRecords'
          },
          500: {
            $ref: '#/definitions/Response/internalServerError'
          }
        }
      },
      delete: {
        tags: ['User'],
        summary: 'Delete User by ID',
        description: 'Delete single User data',
        operationId: 'deleteUser',
        parameters: [
          {
            name: 'id',
            in: 'path',
            description: 'Delete single User data',
            required: true,
            type: 'string'
          },
          { $ref: '#/definitions/Headers/content_type' }
        ],
        responses: {
          200: {
            $ref: '#/definitions/Response/deleteUser'
          },
          400: {
            $ref: '#/definitions/Response/badRequest'
          },
          401: {
            $ref: '#/definitions/Response/unauthorized'
          },
          201: {
            $ref: '#/definitions/Response/noRecords'
          },
          500: {
            $ref: '#/definitions/Response/internalServerError'
          }
        }
      }
    }
  },
  definitions
}
