const jwt = require('express-jwt')
const { ROLES } = require('../libs/constants')
const ClientError = require('../helpers/ClientError')

const openApis = [
  '/api/v1/auth/login'
]
const secret = process.env.JWT_SECRET

const auth = jwt({ secret, algorithms: ['HS256'] }).unless({ path: openApis })

const adminUser = function (req, res, next) {
  if (req.user && req.user.role === ROLES.ADMIN) next()
  else next(new ClientError('Access denied', 403))
}

const validUser = function (req, res, next) {
  if (req.user) next()
  else next(new ClientError('Access denied', 403))
}

const sendMessage = async (message, senderID) => {
  try {
    const accountSid = process.env.TWILIO_ACCOUNT_SID
    const authToken = process.env.TWILIO_TOKEN
    const client = require('twilio')(accountSid, authToken, {
      lazyLoading: true
    })
    const data = await client.messages.create({
      to: senderID, body: message, from: process.env.TWILIO_CONTACT_NUMBER
    })
    if (data) { console.log('message send successfully') } else { console.log('failed to send message') }
  } catch (error) {
    console.log(`Error at sendMessage --> ${error}`)
  }
}

/**
 * Generate random characters
 * @param {number} [length] default 9
 * @param {object} [options]
 * @param {boolean} [options.alpha]
 * @param {boolean} [options.small]
 * @param {boolean} [options.caps]
 * @param {boolean} [options.num]
 * @param {string} [options.special]
 */

/**
 * Validate the given text against provided policy options
 * @param {string} [text]
 * @param {object} [options] validation policy options
 * @param {boolean} [options.alpha]
 * @param {boolean} [options.small]
 * @param {boolean} [options.caps]
 * @param {boolean} [options.num]
 * @param {boolean} [options.special]
 * @param {number} [options.min]
 * @param {number} [options.max]
 */
function validateChars (text, options = {}) {
  if (!(options.min || options.max)) return false
  const a = '(?=.*[A-Za-z])'
  const c = '(?=.*[A-Z])'
  const s = '(?=.*[a-z])'
  const n = '(?=.*\\d)'
  const sp = '(?=.*[^A-Za-z0-9\\s])'

  let chars = ''
  if (options.alpha) chars += a
  if (options.caps) chars += c
  if (options.small) chars += s
  if (options.num) chars += n
  if (options.special) chars += sp

  const min = options.min || ''
  const max = options.max || ''
  const regex = (new RegExp(`^${chars}.{${min},${max}}$`))
  return regex.test(text)
}

function checkPasswordPolicy (password) {
  return validateChars(password, {
    small: true,
    caps: true,
    num: true,
    special: false,
    min: 8
  })
}

module.exports = {
  auth,
  checkPasswordPolicy,
  sendMessage,
  adminUser,
  validUser
}
