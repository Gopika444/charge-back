class ClientError extends Error {
  constructor (message, code, statusCode = 400) {
    super(message)
    this.name = 'ClientError'
    this.code = code
    this.statusCode = statusCode
  }
}

module.exports = ClientError
